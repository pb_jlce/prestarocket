<?php

use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;
use PrestaShop\PrestaShop\Core\Localization\Exception\LocalizationException;

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Módulo PrestaShop
 *
 * Class jl_prestatools
 * @author  Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class jl_prestatools extends Module
{
    protected $openweather_apibaseurl = 'https://api.openweathermap.org/data/2.5/weather?appid=5e841f8cfd43aa27a4d59cf0633972bf';
    protected $openweather_iconurl = 'https://openweathermap.org/img/wn/';

    /**
     * jl_prestatools constructor.
     */
    function __construct()
    {
        $this->name = 'jl_prestatools';
        $this->tab = 'JL_PrestaTools';
        $this->author = 'Jose Lorenzo Cameselle Escribano';
        $this->author_link = 'https://www.linkedin.com/in/joselorenzocameselle/';
        $this->version = '1.0.0';
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('JL PrestaTools', [], 'Modules.JL_PrestaTools.Admin');
        $this->description = $this->trans('Herramientas diversas para PrestaShop', [], 'Modules.JL_PrestaTools.Admin');
        $this->ps_versions_compliancy = ['min' => '1.7.7.x', 'max' => _PS_VERSION_];
        $this->need_instance = 0;
        $this->confirmUninstall = $this->trans('¿Deseas desinstalar el módulo?', [], 'Modules.JL_PrestaTools.Admin');
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public function install(): bool
    {
        $mySQLQuery = "CREATE TABLE IF NOT EXISTS
            " . _DB_PREFIX_ . "jl_prestatools_vouchers(
                customer_id INT(10) UNSIGNED NOT NULL,
                customer_name VARCHAR(255) NOT NULL,
                customer_email VARCHAR(255) NOT NULL,
                cart_rule_code VARCHAR(255) NOT NULL,
                scratched TINYINT(1) UNSIGNED NOT NULL,
                date_add DATETIME DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (customer_id)
            ) ENGINE = " . _MYSQL_ENGINE_ . " DEFAULT CHARSET = UTF8";

        if (Db::getInstance()->execute($mySQLQuery) == false) return false;

        if (parent::install()
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayNav1')
            && $this->registerHook('displayHome')
            && $this->registerHook('displayFooterBefore')
            && $this->registerHook('actionValidateOrder')
            && $this->registerHook('displayOrderConfirmation')
            && $this->registerHook('displayAdminProductsExtra')
            && $this->registerHook('displayProductAdditionalInfo')
            && $this->registerHook('actionAdminControllerSetMedia')
            && $this->registerHook('actionAdminProductsControllerSaveBefore')
        ) {
            $hookDisplayHomeID = Hook::getIdByName('displayHome');
            $hookDisplayFtBfID = Hook::getIdByName('displayFooterBefore');

            if ($hookDisplayHomeID && $hookDisplayFtBfID) {
                $displayHomeCurrentPos = $this->getPosition($hookDisplayHomeID);
                $displayFtBfCurrentPos = $this->getPosition($hookDisplayFtBfID);

                if ($displayHomeCurrentPos > 1) {
                    $this->updatePosition($hookDisplayHomeID, false, 1);
                }

                if ($displayFtBfCurrentPos > 1) {
                    $this->updatePosition($hookDisplayFtBfID, false, 1);
                }
            }

            return true;
        }

        $this->_clearCache('*');

        return false;
    }

    /**
     * @return bool
     */
    public function uninstall(): bool
    {
        $this->_clearCache('*');

        if (Db::getInstance()->execute("DROP TABLE IF EXISTS " . _DB_PREFIX_ . "jl_prestatools_vouchers") == false) return false;

        if (parent::uninstall()
            && $this->unregisterHook('displayHeader')
            && $this->unregisterHook('displayNav1')
            && $this->unregisterHook('displayHome')
            && $this->unregisterHook('displayFooterBefore')
            && $this->unregisterHook('actionValidateOrder')
            && $this->unregisterHook('displayOrderConfirmation')
            && $this->unregisterHook('displayAdminProductsExtra')
            && $this->unregisterHook('displayProductAdditionalInfo')
            && $this->unregisterHook('actionAdminControllerSetMedia')
            && $this->unregisterHook('actionAdminProductsControllerSaveBefore')
        ) {
            Configuration::deleteByName('JL_HOME_MSSG');
            Configuration::deleteByName('JL_FOOTER_BEF_MSSG');
            Configuration::deleteByName('JL_CSV_FILE');
            Configuration::deleteByName('JL_AMOUNT_SPENT_IN_ORDERS');
            Configuration::deleteByName('JL_VOUCHER_AMOUNT');
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function postProcess(): string
    {
        $languages = Language::getLanguages(false);

        if (Tools::isSubmit('submitThisConf')) {
            $values = [];

            foreach ($languages as $lang) {
                $values['JL_HOME_MSSG'][$lang['id_lang']] = Tools::getValue('JL_HOME_MSSG_' . $lang['id_lang']);
                $values['JL_FOOTER_BEF_MSSG'][$lang['id_lang']] = Tools::getValue('JL_FOOTER_BEF_MSSG_' . $lang['id_lang']);
            }

            Configuration::updateValue('JL_HOME_MSSG', $values['JL_HOME_MSSG']);
            Configuration::updateValue('JL_FOOTER_BEF_MSSG', $values['JL_FOOTER_BEF_MSSG']);

            $this->_clearCache('*');

            return $this->displayConfirmation($this->trans('Datos guardados correctamente', [], 'Admin.Notifications.Success'));
        }

        if (Tools::isSubmit('submitImportCSV')) {
            $values = [];

            if (isset($_FILES['JL_CSV_FILE'])
                && isset($_FILES['JL_CSV_FILE']['tmp_name'])
                && !empty($_FILES['JL_CSV_FILE']['tmp_name'])
            ) {
                $file_name = $_FILES['JL_CSV_FILE']['name'];
                $file_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $file_name;

                if (!move_uploaded_file($_FILES['JL_CSV_FILE']['tmp_name'], $file_path)) {
                    return $this->displayError($this->trans('Ha habido un error al intentar subir el fichero', [], 'Admin.Notifications.Error'));
                } else {
                    if (Configuration::get('JL_CSV_FILE') != $file_name) {
                        @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . Configuration::get('JL_CSV_FILE'));
                    }

                    $values['JL_CSV_FILE'] = $file_name;
                }

                Configuration::updateValue('JL_CSV_FILE', $values['JL_CSV_FILE']);

                ## Proceso de importación de artículos desde el fichero CSV subido:
                $csvArr = $fields = [];
                $index = 0;
                $handle = @fopen($file_path, 'r');
                if ($handle) {
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        if (empty($fields)) {
                            $fields = $row;
                            continue;
                        }
                        foreach ($row as $k => $value) {
                            $csvArr[$index][$fields[$k]] = $value;
                        }
                        $index++;
                    }
                    if (!feof($handle)) {
                        return $this->displayError($this->trans('Ha habido un error al intentar leer el fichero', [], 'Admin.Notifications.Error'));
                    }
                    fclose($handle);
                }
                $this->importProductsFromArray($csvArr);
            }

            $this->_clearCache('*');

            return $this->displayConfirmation($this->trans('Importación de productos correcta', [], 'Admin.Notifications.Success'));
        }

        if (Tools::isSubmit('submitVouchersConf')) {
            Configuration::updateValue('JL_AMOUNT_SPENT_IN_ORDERS', Tools::getValue('JL_AMOUNT_SPENT_IN_ORDERS'));
            Configuration::updateValue('JL_VOUCHER_AMOUNT', Tools::getValue('JL_VOUCHER_AMOUNT'));
            $this->_clearCache('*');
            return $this->displayConfirmation($this->trans('Datos guardados correctamente', [], 'Admin.Notifications.Success'));
        }

        return '';
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getContent(): string
    {
        return $this->postProcess()
            . $this->renderMssgsForm()
            . $this->renderImportCSV()
            . $this->renderVouchersForm()
            . $this->renderVouchersList();
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderMssgsForm(): string
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Frases bienvenida', [], 'Modules.JL_PrestaTools.Admin'),
                    'icon' => 'icon-file-text'
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Frase en Inicio', [], 'Modules.JL_PrestaTools.Admin'),
                        'name' => 'JL_HOME_MSSG',
                        'desc' => $this->trans('Introduce un texto para ser mostrado en la página de Inicio, sobre el slider.', [], 'Modules.JL_PrestaTools.Admin')
                    ],
                    [
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Frase sobre el footer', [], 'Modules.JL_PrestaTools.Admin'),
                        'name' => 'JL_FOOTER_BEF_MSSG',
                        'desc' => $this->trans('Texto para ser mostrado sobre el footer.', [], 'Modules.JL_PrestaTools.Admin')
                    ]
                ],
                'submit' => [
                    'title' => $this->trans('Guardar', [], 'Admin.Actions')
                ]
            ],
        ];

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?: 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitThisConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        ];

        return $helper->generateForm([$fields_form]);
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderImportCSV(): string
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Importar productos', [], 'Modules.JL_PrestaTools.Admin'),
                    'icon' => 'icon-upload-alt'
                ],
                'input' => [
                    [
                        'type' => 'file',
                        'label' => $this->trans('Importar CSV', [], 'Modules.JL_PrestaTools.Admin'),
                        'name' => 'JL_CSV_FILE',
                        'hint' => $this->trans('Importar productos desde fichero CSV', [], 'Modules.JL_PrestaTools.Admin')
                    ],
                ],
                'submit' => [
                    'title' => $this->trans('Importar', [], 'Admin.Actions'),
                    'icon' => 'icon-upload-alt'
                ],
            ],
        ];

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?: 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitImportCSV';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'uri' => $this->getPathUri(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        ];

        return $helper->generateForm([$fields_form]);
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderVouchersForm(): string
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Cupones por dinero gastado', [], 'Modules.JL_PrestaTools.Admin'),
                    'icon' => 'icon-tags'
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->trans('Gasto a alcanzar', [], 'Modules.JL_PrestaTools.Admin'),
                        'name' => 'JL_AMOUNT_SPENT_IN_ORDERS',
                        'desc' => $this->trans('Cantidad de dinero (impuestos incluidos) a alcanzar en el total de pedidos por cliente.', [], 'Modules.JL_PrestaTools.Admin')
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->trans('Importe del cupón', [], 'Modules.JL_PrestaTools.Admin'),
                        'name' => 'JL_VOUCHER_AMOUNT',
                        'desc' => $this->trans('Importe del cupón a generar al alcanzar la cantidad anterior.', [], 'Modules.JL_PrestaTools.Admin')
                    ]
                ],
                'submit' => [
                    'title' => $this->trans('Guardar', [], 'Admin.Actions')
                ]
            ],
        ];

        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');
        $lang = new Language($defaultLang);

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?: 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitVouchersConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigVoucherFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        ];

        return $helper->generateForm([$fields_form]);
    }

    /**
     * @return array
     */
    public function getConfigFieldsValues(): array
    {
        $languages = Language::getLanguages(false);
        $fields = [];

        foreach ($languages as $lang) {
            $fields['JL_HOME_MSSG'][$lang['id_lang']] = Tools::getValue('JL_HOME_MSSG_' . $lang['id_lang'], Configuration::get('JL_HOME_MSSG', $lang['id_lang']));
            $fields['JL_FOOTER_BEF_MSSG'][$lang['id_lang']] = Tools::getValue('JL_FOOTER_BEF_MSSG_' . $lang['id_lang'], Configuration::get('JL_FOOTER_BEF_MSSG', $lang['id_lang']));
        }

        return $fields;
    }

    /**
     * @return array
     */
    public function getConfigVoucherFieldsValues(): array
    {
        return [
            'JL_AMOUNT_SPENT_IN_ORDERS' => Tools::getValue('JL_AMOUNT_SPENT_IN_ORDERS', Configuration::get('JL_AMOUNT_SPENT_IN_ORDERS')),
            'JL_VOUCHER_AMOUNT' => Tools::getValue('JL_VOUCHER_AMOUNT', Configuration::get('JL_VOUCHER_AMOUNT'))
        ];
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function renderVouchersList(): string
    {
        $mySQLQuery = "SELECT * FROM " . _DB_PREFIX_ . "jl_prestatools_vouchers";
        $jlVouchersArr = Db::getInstance()->executeS($mySQLQuery);

        if (!is_array($jlVouchersArr) || empty($jlVouchersArr)) return '';

        $html = '<div class="jl_vouchers_list_container"><div class="panel-heading"><i class="icon-list"></i>';
        $html .= $this->trans('Listado de cupones generados', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</div>';
        $html .= '<div class="table-responsive-row clearfix"><table id="table-jl_vouchers" class="table jl_vouchers">';
        $html .= '<thead><tr class="nodrag nodrop"><th class="fixed-width-xs center">';
        $html .= '<span class="title_box active">';
        $html .= $this->trans('ID Cliente', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</span></th><th class="fixed-width-lg"><span class="title_box">';
        $html .= $this->trans('Nombre y apellidos', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</span></th><th class="fixed-width-lg"><span class="title_box">';
        $html .= $this->trans('Email', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</span></th><th class="fixed-width-sm"><span class="title_box">';
        $html .= $this->trans('Código Cupón', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</span></th><th class="fixed-width-lg"><span class="title_box">';
        $html .= $this->trans('Fecha de creación', [], 'Modules.JL_PrestaTools.Admin');
        $html .= '</span></th></tr></thead>';

        $html .= '<tbody>';
        foreach ($jlVouchersArr as $eachVoucherArr) {
            $html .= '<tr class="odd"><td class="pointer fixed-width-xs center">';
            $html .= $eachVoucherArr['customer_id'];
            $html .= '</td><td class="pointer fixed-width-lg">';
            $html .= $eachVoucherArr['customer_name'];
            $html .= '</td><td class="pointer fixed-width-lg">';
            $html .= $eachVoucherArr['customer_email'];
            $html .= '</td><td class="pointer fixed-width-sm">';
            $html .= $eachVoucherArr['cart_rule_code'];
            $html .= '</td><td class="pointer fixed-width-lg">';
            $html .= $eachVoucherArr['date_add'];
            $html .= '</td></tr>';
        }
        $html .= '</tbody>';

        $html .= '</table></div></div>';

        return $html;
    }

    public function hookDisplayHeader(): void
    {
        $this->context->controller->registerStylesheet(
            'modules-jl_prestatools',
            'modules/' . $this->name . '/views/css/jl_prestatools.css',
            [
                'media' => 'all',
                'priority' => 150
            ]
        );
        $this->context->controller->registerJavascript(
            'modules-jl_prestatools',
            'modules/' . $this->name . '/views/js/lib/scratch-card.js',
            [
                'position' => 'bottom',
                'priority' => 150
            ]
        );
    }

    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addCSS($this->_path . 'views/admin/css/jl_prestatools.css');
        $this->context->controller->addJS($this->_path . 'views/admin/js/jl_prestatools.js');
    }

    /**
     * @return false|string
     */
    public function hookDisplayNav1()
    {
        try {
            $geoIP2Data = $this->getGeoIp2Data();
            $thisCity = $geoIP2Data["city_name"];
            $thisCountryISOCode = $geoIP2Data["country_iso_code"];
            $thisCityRequest = "$thisCity,$thisCountryISOCode";
            $apiRequest = $this->openweather_apibaseurl . "&q=" . $thisCityRequest;
            $requestResult = file_get_contents($apiRequest);
            $weatherData = json_decode($requestResult);

            if ($thisCountryISOCode === 'ES') {
                $thisLon = str_replace('.', ',', $weatherData->coord->lon);
                $thisLat = str_replace('.', ',', $weatherData->coord->lat);
            } else {
                $thisLon = $weatherData->coord->lon;
                $thisLat = $weatherData->coord->lat;
            }

            $weatherIcon = $this->openweather_iconurl . $weatherData->weather[0]->icon . '.png';
            $currentWeatherData = [
                "city" => $thisCity,
                "lon" => $thisLon,
                "lat" => $thisLat,
                "temp" => ($weatherData->main->temp + 0) - 273.15,
                "tempmax" => ($weatherData->main->temp_max + 0) - 273.15,
                "tempmin" => ($weatherData->main->temp_min + 0) - 273.15,
                "pressure" => ($weatherData->main->pressure + 0),
                "humidity" => ($weatherData->main->humidity + 0),
                "wind_speed" => ($weatherData->wind->speed + 0),
                "sky_condition" => $weatherData->weather[0]->main,
                "icon" => $weatherIcon,
                "description" => $weatherData->weather[0]->description
            ];

            $this->smarty->assign([
                "geoIP2Data" => $geoIP2Data,
                "currentWeather" => $currentWeatherData
            ]);
        } catch (InvalidDatabaseException $excep) {
            PrestaShopLogger::addLog($excep, 3);
        }

        return $this->display(__FILE__, 'views/templates/hook/displayNav.tpl');
    }

    public function hookDisplayHome()
    {
        $thisLangId = $this->context->language->id ?? 1;
        $dispHomeMssg = Configuration::get('JL_HOME_MSSG', $thisLangId);
        $this->smarty->assign('homeMssg', $dispHomeMssg);

        return $this->display(__FILE__, 'views/templates/hook/displayHome.tpl');
    }

    public function hookDisplayFooterBefore()
    {
        $thisLangId = $this->context->language->id ?? 1;
        $dispFooterBefMssg = Configuration::get('JL_FOOTER_BEF_MSSG', $thisLangId);
        $this->smarty->assign('footerBefMssg', $dispFooterBefMssg);

        return $this->display(__FILE__, 'views/templates/hook/displayFooterBefore.tpl');
    }

    /**
     * @param array $productsArray
     * @return void
     */
    protected function importProductsFromArray(array $productsArray): void
    {
        foreach ($productsArray as $productRaw) {
            $tax_rate = (int)$productRaw['IVA'];
            switch ($tax_rate) {
                case 10:
                    $id_tax_rules_group = 2;
                    break;
                case 4:
                    $id_tax_rules_group = 3;
                    break;
                default:
                    $id_tax_rules_group = 1;
            }

            $info = [
                "name" => $productRaw['Nombre'],
                "reference" => $productRaw['Referencia'],
                "ean13" => $productRaw['EAN13'],
                "wholesale_price" => ($productRaw['Precio de coste'] + 0),
                "id_tax_rules_group" => $id_tax_rules_group,
                "price_tin" => ($productRaw['Precio de venta'] + 0),
                "quantity" => ($productRaw['Cantidad'] + 0),
                "category" => str_replace(";", ",", $productRaw['Categorias']),
                "manufacturer" => $productRaw['Marca']
            ];

            (new AdminImportController)->productImportCustom($info);
        }
    }

    /**
     * @return array
     * @throws InvalidDatabaseException
     */
    public function getGeoIp2Data(): array
    {
        $geoData = [];

        if (!in_array(Tools::getRemoteAddr(), ['localhost', '127.0.0.1', '::1'])) {
            if (@filemtime(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_)) {
                $reader = new GeoIp2\Database\Reader(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_);

                try {
                    $record = $reader->city(Tools::getRemoteAddr());
                } catch (AddressNotFoundException $e) {
                    $record = null;
                }

                if (is_object($record) && Validate::isLanguageIsoCode($record->country->isoCode)
                    && (int)Country::getByIso(strtoupper($record->country->isoCode)) != 0
                ) {
                    $geoData["country_iso_code"] = $record->country->isoCode;
                    $geoData["country_name"] = $record->country->name;
                    $geoData["city_name"] = $record->city->name;
                    $geoData["postal_code"] = $record->postal->code;
                    $geoData["subdivision"] = $record->mostSpecificSubdivision->name;
                }


            }
        }

        return $geoData;
    }

    /**
     * @param $params
     * @return false|string
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $thisLangId = $this->context->language->id ?? 1;
        $id_product = (int)$params['id_product'];
        $this->smarty->assign([
            'jl_customized_text' => Configuration::get('JL_PRODUCT_TEXT_' . $id_product, $thisLangId),
            'lang_id' => $thisLangId,
            'lang_code' => $this->context->language->getLanguageCode(),
            'jl_id_product' => $id_product
        ]);
        return $this->display(__FILE__, 'views/templates/hook/displayAdmProdsExtra.tpl');
    }

    public function hookActionAdminProductsControllerSaveBefore()
    {
        $languages = Language::getLanguages(false);
        $id_product = (int)$_REQUEST['jl_id_product'];
        $values = [];

        foreach ($languages as $lang) {
            $values['JL_PRODUCT_TEXT_' . $id_product][$lang['id_lang']] = $_REQUEST['form_jl_customized_text'][$lang['id_lang']];
        }

        Configuration::updateValue('JL_PRODUCT_TEXT_' . $id_product, $values['JL_PRODUCT_TEXT_' . $id_product]);
    }

    /**
     * @return false|string
     */
    public function hookDisplayProductAdditionalInfo()
    {
        $thisLangId = $this->context->language->id ?? 1;
        $templateVars = $this->context->smarty->getTemplateVars();
        if (isset($templateVars['product']['id'])) {
            $id_product = (int)$templateVars['product']['id'];
            $jl_custom_text = Configuration::get('JL_PRODUCT_TEXT_' . $id_product, $thisLangId);
            if (!empty($jl_custom_text)) {
                $this->smarty->assign([
                    'jl_customized_text' => Configuration::get('JL_PRODUCT_TEXT_' . $id_product, $thisLangId)
                ]);
            }
        }
        return $this->display(__FILE__, 'views/templates/hook/displayProductAdditionalInfo.tpl');
    }

    /**
     * @return false|string
     * @throws LocalizationException
     */
    public function hookDisplayOrderConfirmation()
    {
        $id_customer = $this->context->customer->id;
        $noScratchedVoucher = $this->getNoScratchedVoucherCode($id_customer);

        if (is_string($noScratchedVoucher) && $noScratchedVoucher !== '') {
            $this->smarty->assign('jl_voucher_code', $noScratchedVoucher);

            $cart_rule_array = CartRule::getCartsRuleByCode($noScratchedVoucher, $this->context->language->id);
            $iso_code = $this->context->currency->iso_code;
            $reduction_amount = $this->context->currentLocale->formatPrice($cart_rule_array[0]['reduction_amount'], $iso_code);
            $this->smarty->assign('jl_voucher_amount', $reduction_amount);

            $sqlDBQuery = /** @lang MySQL DB query */
                "UPDATE " . _DB_PREFIX_ . "jl_prestatools_vouchers
                SET scratched = 1
                WHERE customer_id = $id_customer
                AND cart_rule_code = '$noScratchedVoucher'
                AND scratched = 0";
            Db::getInstance()->execute($sqlDBQuery);
        }

        return $this->display(__FILE__, 'views/templates/hook/displayOrderConfirmation.tpl');
    }

    /**
     * @throws LocalizationException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionValidateOrder()
    {
        $id_customer = $this->context->customer->id;
        $totalInOrders = $this->getCustomerTotalInOrders($id_customer);

        if ($totalInOrders > 0) {
            $iso_code = $this->context->currency->iso_code;
            $amount_in_orders = $this->context->currentLocale->formatPrice($totalInOrders, $iso_code);
            $amount_for_voucher = (Configuration::get('JL_AMOUNT_SPENT_IN_ORDERS')) * 1;
            $alreadyWithVoucher = $this->customerWithVoucher($id_customer);

            $emailData = [
                '{firstname}' => $this->context->customer->firstname,
                '{lastname}' => $this->context->customer->lastname,
                '{amount_in_orders}' => $amount_in_orders
            ];

            $templPath = dirname(__FILE__)
                . DIRECTORY_SEPARATOR . 'mails'
                . DIRECTORY_SEPARATOR . 'es'
                . DIRECTORY_SEPARATOR;

            if (Validate::isEmail($this->context->customer->email)) {
                Mail::Send(
                    $this->context->language->id ?? 1,
                    'amount_in_orders',
                    $this->context->getTranslator()->trans('Su total en pedidos', [], 'Emails.Subject', $this->context->language->locale),
                    $emailData,
                    $this->context->customer->email,
                    $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                    null,
                    null,
                    null,
                    null,
                    $templPath,
                    false,
                    $this->context->shop->id
                );
            }

            if ($totalInOrders >= $amount_for_voucher && !$alreadyWithVoucher) $this->generateVoucherForCustomer($this->context);
        }
    }

    /**
     * @param int $id_customer
     * @return float|int
     */
    protected function getCustomerTotalInOrders(int $id_customer)
    {
        $dbQuery = /** @lang MySQL DB query */
            "SELECT
                IFNULL(SUM(total_paid), 0) \"total_orders\"
            FROM " . _DB_PREFIX_ . "orders
            WHERE id_customer = $id_customer
            AND current_state NOT IN (6, 7, 8)";

        $total_orders = Db::getInstance()->getValue($dbQuery);

        if ($total_orders && is_numeric($total_orders) && (float)$total_orders > 0) {
            return (float)$total_orders;
        }

        return 0;
    }

    /**
     * @param int $id_customer
     * @return bool
     */
    protected function customerWithVoucher(int $id_customer): bool
    {
        $dbQuery = /** @lang MySQL DB query */
            "SELECT
                COUNT(*) \"count_jl_vouchers\"
            FROM " . _DB_PREFIX_ . "jl_prestatools_vouchers
            WHERE customer_id = $id_customer";

        return (bool)Db::getInstance()->getValue($dbQuery);
    }

    /**
     * @param int $id_customer
     * @return false|string|null
     */
    protected function getNoScratchedVoucherCode(int $id_customer)
    {
        $dbQuery = /** @lang MySQL DB query */
            "SELECT
                cart_rule_code
            FROM " . _DB_PREFIX_ . "jl_prestatools_vouchers
            WHERE customer_id = $id_customer
            AND scratched = 0";

        return Db::getInstance()->getValue($dbQuery);
    }

    /**
     * @param Context $context
     * @throws LocalizationException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    protected function generateVoucherForCustomer(Context $context): void
    {
        $voucherCode = strtoupper(Tools::passwdGen());
        $customerName = $context->customer->firstname . ' ' . $context->customer->lastname;
        $customerMail = $context->customer->email;
        $customerId = $context->customer->id;
        $thisDisplayName = $this->displayName;

        $dbSQLQuery = /** @lang MySQL DB Query */
            "INSERT IGNORE
            INTO " . _DB_PREFIX_ . "jl_prestatools_vouchers
                (customer_id, customer_name, customer_email, cart_rule_code)
            VALUES ($customerId, '$customerName', '$customerMail', '$voucherCode')";

        if (Db::getInstance()->execute($dbSQLQuery)) {
            $voucherReductionAmount = Configuration::get('JL_VOUCHER_AMOUNT') ?? 0;

            if (!empty($voucherReductionAmount)) {
                $cartRule = new CartRule();
                $cartRule->id_customer = $customerId;
                $cartRule->date_from = date('Y-m-d H:i:s', time());
                $cartRule->date_to = date('Y-m-d H:i:s', strtotime('+10 years'));
                $cartRule->name = [$context->language->id => $voucherCode];
                $cartRule->quantity = 1;
                $cartRule->quantity_per_user = 1;
                $cartRule->partial_use = 0;
                $cartRule->code = $voucherCode;
                $cartRule->reduction_amount = $voucherReductionAmount;
                $cartRule->highlight = 1;
                $addedCarRule = $cartRule->add();

                if ($addedCarRule) {
                    $iso_code = $context->currency->iso_code;
                    $voucher_amount = $context->currentLocale->formatPrice($voucherReductionAmount, $iso_code);
                    $templPath = dirname(__FILE__)
                        . DIRECTORY_SEPARATOR . 'mails'
                        . DIRECTORY_SEPARATOR . 'es'
                        . DIRECTORY_SEPARATOR;

                    if (Validate::isEmail($customerMail)) {
                        Mail::Send(
                            $context->language->id ?? 1,
                            'voucher',
                            $context->getTranslator()->trans(
                                '¡Ha conseguido un cupón de descuento!',
                                [],
                                'Emails.Subject',
                                $context->language->locale),
                            [
                                '{voucher_num}' => $voucherCode,
                                '{voucher_amount}' => $voucher_amount,
                                '{firstname}' => $context->customer->firstname,
                                '{lastname}' => $context->customer->lastname
                            ],
                            $customerMail,
                            $customerName,
                            null,
                            null,
                            null,
                            null,
                            $templPath,
                            false,
                            $context->shop->id
                        );
                    }
                } else {
                    $dbSQLDelQuery = /** @lang MySQL DB Query */
                        "DELETE
                        FROM " . _DB_PREFIX_ . "jl_prestatools_vouchers
                        WHERE customer_id = $customerId
                        AND cart_rule_code = '$voucherCode'";

                    $errorMssg = "Módulo $thisDisplayName: error al intentar eliminar el cupón $voucherCode en la tabla " . _DB_PREFIX_ . "jl_prestatools_vouchers";
                    Db::getInstance()->execute($dbSQLDelQuery) || PrestaShopLogger::addLog($errorMssg, 3);
                }
            }
        } else {
            $errorMssg = "Módulo $thisDisplayName: no ha sido posible registrar el cupón $voucherCode en la tabla " . _DB_PREFIX_ . "jl_prestatools_vouchers";
            PrestaShopLogger::addLog($errorMssg, 3);
        }
    }
}
