<?php

/**
 * Class AdminImportController
 *
 * @override AdminImportControllerCore
 * @author  Jose Lorenzo Cameselle Escribano <lorenzo.cameselle@gmail.com>
 */
class AdminImportController extends AdminImportControllerCore
{
    /**
     * @param array $info
     */
    public function productImportCustom(array $info)
    {
        $this->productImportOne(
            $info,
            1,
            1,
            false,
            true,
            false,
            null,
            false,
            $accesories
        );
    }


}

