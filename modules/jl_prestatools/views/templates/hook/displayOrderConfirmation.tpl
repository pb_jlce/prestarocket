{if isset($jl_voucher_code) && isset($jl_voucher_amount)}
    <div class="container jl_prestatools displayOrderConfirmation">
        <div class="jl_voucher_title">
            <h2>¡¡ RASCA Y GANA !!</h2>
        </div>
        <div class="jl_card">
            <div class="jl_base">
                <span id="jl_voucher_label" class="jl_hide"><strong>¡¡ CUPÓN !!</strong></span>
                <span id="jl_voucher_amount" class="jl_hide">CÓDIGO: <b>{$jl_voucher_code}</b></span>
                <span id="jl_voucher_code" class="jl_hide">VALOR: <b>{$jl_voucher_amount}</b></span>
            </div>
            <img src="{$base_url}modules/jl_prestatools/views/img/gift-card.png" alt="Gift card"/>
            <canvas id="scratch"></canvas>
        </div>
    </div>
{/if}