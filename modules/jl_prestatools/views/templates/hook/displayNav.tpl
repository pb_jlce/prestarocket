{if isset($currentWeather.city)}
    <div class="container jl_prestatools dispNav">
        <p>
            {$currentWeather.city},
            &nbsp;&nbsp;{$currentWeather.temp} ºC,
            &nbsp;&nbsp;<i class="material-icons">opacity</i> {$currentWeather.humidity} %,
            &nbsp;&nbsp;Cielo: <img class="jl-weather-icon" src="{$currentWeather.icon}" title="{$currentWeather.sky_condition}" alt="{$currentWeather.sky_condition}" />
        </p>
    </div>
{/if}