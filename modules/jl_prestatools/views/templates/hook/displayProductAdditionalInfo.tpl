{if isset($jl_customized_text)}
    <div class="container jl_prestatools displayProductAdditionalInfo">
        <h2>{l s='Info extra' d='Modules.JL_PrestaTools.Shop'}</h2>
        <p>{$jl_customized_text}</p>
    </div>
{/if}
