{if isset($jl_customized_text)}
    <div data-locale="{$lang_code}" class="jl_prestatools row">
        <fieldset class="col-md-6 form-group">
            <label class="form-control-label translation-label-{$lang_code}">Texto Personalizado</label>
            <input type="text" id="form_jl_customized_text" name="form_jl_customized_text[{$lang_id}]"
                   class="form-control"
                   placeholder="Indique un texto personalizado a mostrar en la ficha del producto."
                   value="{$jl_customized_text}"/>
            <input type="hidden" id="jl_id_product" name="jl_id_product" value="{$jl_id_product}">
        </fieldset>
    </div>
{/if}
